#!/bin/bash


SCRIPTS="/opt/scripts"
WALLPAPER_URL="https://gitlab.com/online_technical_test/pegadaian/-/raw/main/pegadaian.jpg?ref_type=heads"
WALLPAPER_PATH="$SCRIPTS/pegadaian.jpg"
WALLPAPER_FINAL_PATH="/usr/share/backgrounds/pegadaian.jpg"
LAST_MODIFIED_PATH="$SCRIPTS/.wallpaper_last_modified"
OLD_VERSION="$SCRIPTS/.wallpaper_old_version"


# wget --timestamping -q -O "$WALLPAPER_PATH" "$WALLPAPER_URL"

if [ $? -eq 0 ] && [ -f "$WALLPAPER_PATH" ]; then
    LAST_MODIFIED=$(wget --spider --server-response "$WALLPAPER_URL" 2>&1 | grep "Date" | cut -d' ' -f3-)

    if [ -n "$LAST_MODIFIED" ]; then
        CODE_NOW=`curl -sL $WALLPAPER_URL | md5sum | cut -d ' ' -f 1`
        if [[ ! -f $OLD_VERSION ]]; then
            echo -n $CODE_NOW > $OLD_VERSION
            sudo chown -R supportadmin. $OLD_VERSION
        else
            echo "file exist"
        fi
        if [[ -f $OLD_VERSION ]]; then
            CODE_THEN=`cat $OLD_VERSION`
            if [[ $CODE_NOW == $CODE_THEN ]]; then
                echo "version equal"
            elif [[ $CODE_NOW != $CODE_THEN ]]; then
                wget -O $WALLPAPER_PATH $WALLPAPER_URL
                echo "image downloaded"
                if [[ $(file -b $WALLPAPER_PATH) =~ JPEG  ]]; then
                    sudo cp $WALLPAPER_PATH $WALLPAPER_FINAL_PATH
                    echo -n $CODE_NOW > $OLD_VERSION
                else
                    echo "background updated"
                fi
            else
                echo "skip again"
            fi
        else
            echo "skip"
        fi
        if [ ! -f "$LAST_MODIFIED_PATH" ] || [ "$(cat "$LAST_MODIFIED_PATH")" != "$LAST_MODIFIED" ]; then
            echo "$LAST_MODIFIED" > "$LAST_MODIFIED_PATH"
            sudo chown -R supportadmin. $LAST_MODIFIED_PATH
            echo "Wallpaper applied successfully."
        else
            echo "Wallpaper is already up to date."
        fi
    else
        echo "Failed to retrieve Last-Modified header."
    fi
else
    echo "Wallpaper URL does not exist or failed to download."
fi